﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users;

namespace SqlDB
{
    class SqlContext:DbContext
    {


        public SqlContext() : base("name=LocalFileConnection")
        {
            AppDomain.CurrentDomain.SetData("DataDirectory", System.IO.Directory.GetCurrentDirectory());
        }


        public DbSet<RegisteredUser> RegisteredUsers { get; set; }
        public DbSet<ConnectedUser> ConnectedUsers { get; set; }
        public DbSet<Message> Messages { get; set; }
    }
}
