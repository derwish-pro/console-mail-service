﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users;

namespace SqlDB
{
    public class SqlConnectedUsersDb : IConnectedUsersDb
    {
        public bool AddUser(string userName, string token)
        {
            var context = new SqlContext();
            //эта строчка нужна потому что мы прописали RegisteredUser-а из другого контекста
            RegisteredUser registeredUser = (from user in context.RegisteredUsers
                                             where user.Name == userName
                                             select user).FirstOrDefault();

            ConnectedUser connectedUser = new ConnectedUser();
            connectedUser.Name = registeredUser.Name;
            connectedUser.RegisteredUser = registeredUser;
            connectedUser.Token = token;
            connectedUser.LoginTime = DateTime.Now;
            registeredUser.LastLoginDate = connectedUser.LoginTime;

            context.ConnectedUsers.Add(connectedUser);
            context.SaveChanges();

            return true;
        }

        public bool RemoveUser(string userName)
        {
            var context = new SqlContext();

            ConnectedUser connectedUser = (from user in context.ConnectedUsers
                                             where user.Name == userName
                                             select user).FirstOrDefault();

            context.ConnectedUsers.Remove(connectedUser);
            context.SaveChanges();

            return true;
        }

        public ConnectedUser GetUser(string userName)
        {
            var context = new SqlContext();

            ConnectedUser connectedUser = (from user in context.ConnectedUsers
                                           where user.Name == userName
                                           select user).FirstOrDefault();

            return connectedUser;
        }

        public List<ConnectedUser> GetAllUsers()
        {
            var context = new SqlContext();
            List<ConnectedUser> users = context.ConnectedUsers.ToList();

            return users;
        }

        public bool DropDatabase()
        {
            var context = new SqlContext();
            return context.Database.Delete();
        }
    }
}
