﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlDB;
using Users;

namespace SqlDB
{
    public class SqlMailDb : IMailDb
    {
        public bool CreateNewMessage(string fromUserName, string toUserName, string text)
        {
            var context = new SqlContext();

            RegisteredUser fromUser = (from user in context.RegisteredUsers
                                       where user.Name == fromUserName
                                       select user).FirstOrDefault();

            RegisteredUser toUser = (from user in context.RegisteredUsers
                                       where user.Name == toUserName
                                       select user).FirstOrDefault();

            if (fromUser == null || toUser == null) return false;

            Message message = new Message();
            message.Received = false;
            message.Text = text;
            message.fromUser = fromUser;
            message.toUser = toUser;
            message.Date = DateTime.Now;

            context.Messages.Add(message);
            context.SaveChanges();

            return true;
        }



        public List<Message> GetNewMessages(string userName)
        {
            var context = new SqlContext();

            RegisteredUser user = (from u in context.RegisteredUsers
                                   where u.Name == userName
                                       select u).FirstOrDefault();


            if (user == null) return null;

            var mes = from m in context.Messages
                where m.toUser.Name == userName && m.Received == false 
                select m;

            List<Message> messages = mes.ToList();

            return messages;
        }

        public List<Message> GetAllMessages(string userName)
        {
            var context = new SqlContext();

            RegisteredUser user = (from u in context.RegisteredUsers
                                   where u.Name == userName
                                   select u).FirstOrDefault();


            if (user == null) return null;

            var mes = from m in context.Messages
                      where m.toUser.Name == userName
                      select m;

            List<Message> messages = mes.ToList();

            return messages;
        }

        public bool DeleteMessage(int messageId)
        {
            var context = new SqlContext();

            Message message = (from m in context.Messages
                               where m.Id == messageId
                                   select m).FirstOrDefault();


            if (message == null) return false;

            context.Messages.Remove(message);
            context.SaveChanges();

            return true;
        }

        public bool SetMessageReaded(int messageId)
        {
            var context = new SqlContext();

            Message message = (from m in context.Messages
                               where m.Id == messageId
                               select m).FirstOrDefault();


            if (message == null) return false;

            message.Received = true;
            context.SaveChanges();

            return true;
        }
    }


}
