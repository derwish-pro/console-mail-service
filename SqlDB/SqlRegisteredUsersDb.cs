﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users;
using SqlDB;

namespace SqlDB
{
    public class SqlRegisteredUsersDb : IRegisteredUsersDB
    {

        public bool CreateNewUser(string userName, string password)
        {
            RegisteredUser newRegisteredUser = new RegisteredUser();
            newRegisteredUser.Name = userName;
            newRegisteredUser.Password = password;
            newRegisteredUser.RegistrationDate = DateTime.Now;

            var context = new SqlContext();
            context.RegisteredUsers.Add(newRegisteredUser);
            context.SaveChanges();

            return true;
        }

        public bool DeleteUser(string userName)
        {
            var context = new SqlContext();

            RegisteredUser registeredUser = (from user in context.RegisteredUsers
                                             where user.Name == userName
                                             select user).FirstOrDefault();

            if (registeredUser == null) return false;

            context.RegisteredUsers.Remove(registeredUser);
            context.SaveChanges();

            return true;
        }

        public RegisteredUser GetUser(string userName)
        {
            var context = new SqlContext();

            RegisteredUser registeredUser = (from user in context.RegisteredUsers
                                             where user.Name == userName
                                             select user).FirstOrDefault();

            return registeredUser;
        }

        public bool ChangeLoginDate(string userName, DateTime datetime)
        {
            var context = new SqlContext();

            RegisteredUser registeredUser = (from user in context.RegisteredUsers
                                             where user.Name == userName
                                             select user).FirstOrDefault();

            if (registeredUser == null) return false;

            registeredUser.LastLoginDate = datetime;

            context.SaveChanges();

            return true;
        }

        public bool ChangePassword(string userName, string password)
        {
            var context = new SqlContext();

            RegisteredUser registeredUser = (from user in context.RegisteredUsers
                                             where user.Name == userName
                                             select user).FirstOrDefault();

            if (registeredUser == null) return false;

            registeredUser.Password = password;

            context.SaveChanges();

            return true;
        }

        public List<RegisteredUser> GetAllUsers()
        {
            var context = new SqlContext();
            //context.Database.CreateIfNotExists();
            //context.Database.Initialize(true);
            return context.RegisteredUsers.ToList();
        }

        public bool DropDatabase()
        {
            var context = new SqlContext();
            return context.Database.Delete();
        }
    }
}
