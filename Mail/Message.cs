﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using Users;

namespace Mail
{
    public class Message
    {
        public int Id { get; set; }
        public bool Received { get; set; }
        public DateTime? Date { get; set; }
        [Required]
        public RegisteredUser fromUser { get; set; }
        [Required]
        public RegisteredUser toUser { get; set; }
        public string Text{ get; set; }
    }
}
