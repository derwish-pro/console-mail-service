﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Users;

namespace Mail
{
    public class MailManager
    {
        private UsersManager usersManager;
        private IMailDb mailDb;
        public MailManager(UsersManager usersManager, IMailDb mailDb)
        {
            this.usersManager = usersManager;
            this.mailDb = mailDb;
            SetPrintLogMethodsToConsole();
        }

        #region Logs

        public delegate void PrintGoodLogDelegate(string message);
        public delegate void PrintErrorLogDelegate(string message);

        private PrintGoodLogDelegate printGoodLogMethod;
        private PrintErrorLogDelegate printErrorLogMethod;

        public void SetPrintGoodLogMethod(PrintGoodLogDelegate deletate)
        {
            printGoodLogMethod = deletate;
        }

        public void SetPrintLogMethodsToConsole()
        {
            SetPrintErrorLogMethod(message =>
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
            });

            SetPrintGoodLogMethod(message =>
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
            });
        }



        public void SetPrintErrorLogMethod(PrintErrorLogDelegate deletate)
        {
            printErrorLogMethod = deletate;
        }

        private void PrintGoodLog(string messgae)
        {
            if (printGoodLogMethod != null)
                printGoodLogMethod(messgae);
        }

        private void PrintErrorLog(string messgae)
        {
            if (printErrorLogMethod != null)
                printErrorLogMethod(messgae);
        }

        #endregion

        private bool CheckToken(string userName, string token)
        {
            bool result = usersManager.CheckToken(userName, token);
            if (!result) PrintErrorLog(String.Format("User \"{0}\" token is incorrect.", userName));

            return result;
        }

        public bool SendNewMessage(string fromUser, string toUser, string text, string token)
        {
            if (CheckToken(fromUser, token) == false) return false;

            bool result = mailDb.CreateNewMessage(fromUser, toUser, text);
            if (result) PrintGoodLog(String.Format("Message from \"{0}\" to \"{1}\" sended.", fromUser, toUser));
            else PrintErrorLog(String.Format("Can`t send message from \"{0}\" to \"{1}\".", fromUser, toUser));

            return result;
        }

        public int GetNewMessagesCount(string userName, string token)
        {
            if (CheckToken(userName, token) == false) return 0;

            List<Message> messages = mailDb.GetNewMessages(userName);

            return messages.Count;
        }

        public List<MessageSimple> GetNewMessages(string userName, string token)
        {
            if (CheckToken(userName, token) == false) return null;

            List<Message> messages = mailDb.GetNewMessages(userName);

            List<MessageSimple> simpleMessages = ConvertMessageListToSimple(messages);

            return simpleMessages;
        }

        /// <summary>
        /// Эта процедура скрывает приватные данные пользоваетля из сообщения
        /// запрещая к ним прямой доступ
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private MessageSimple ConvertMessageToSimple(Message message)
        {
            MessageSimple messageSimple = new MessageSimple();
            messageSimple.Date = message.Date;
            messageSimple.Id = message.Id;
            messageSimple.Received = message.Received;
            messageSimple.fromUser = message.fromUser.Name;
            messageSimple.toUser = message.toUser.Name;
            messageSimple.Text = message.Text;
            return messageSimple;
        }

        /// <summary>
        /// Эта процедура скрывает приватные данные пользоваетля из сообщения
        /// запрещая к ним прямой доступ
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private List<MessageSimple> ConvertMessageListToSimple(List<Message> messages)
        {
            List<MessageSimple> newList = new List<MessageSimple>();
            foreach (var message in messages)
            {
                MessageSimple newMessage = ConvertMessageToSimple(message);
                newList.Add(newMessage);
            }

            return newList;
        }

        public List<MessageSimple> GetAllMessages(string userName, string token)
        {
            if (CheckToken(userName, token) == false) return null;

            List<Message> messages = mailDb.GetAllMessages(userName);

            List<MessageSimple> simpleMessages = ConvertMessageListToSimple(messages);

            return simpleMessages;
        }

        public bool DeleteMessage(string userName, int messageId, string token)
        {
            if (CheckToken(userName, token) == false) return false;

            bool result = mailDb.DeleteMessage(messageId);
            if (result) PrintGoodLog(String.Format("Message to \"{0}\" deleted.", userName));
            else PrintErrorLog(String.Format("Can`t delete message to \"{0}\".", userName));

            return result;
        }

        public bool SetMessageReaded(string userName, int messageId, string token)
        {
            if (CheckToken(userName, token) == false) return false;

            bool result = mailDb.SetMessageReaded(messageId);
            if (result) PrintGoodLog(String.Format("Message to \"{0}\" deleted.", userName));
            else PrintErrorLog(String.Format("Can`t delete message to \"{0}\".", userName));

            return result;
        }

    }
}
