﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Text;
using System.Threading.Tasks;
using Mail;
using Users;
using SqlDB;

namespace MailService
{
    static class ConsoleUI
    {
        private static UsersManager usersManager;
        private static MailManager mailManager;
        private static bool logined = false;
        private static string username;
        private static string token;
        public static void Start(UsersManager usersManager, MailManager mailManager)
        {
            ConsoleUI.usersManager = usersManager;
            ConsoleUI.mailManager = mailManager;
            Console.Clear();
            Console.WriteLine("Welcome!");
            MainMenu();
        }
        private static void MainMenu()
        {
            while (true)
            {
                if (!logined)
                {
                    Console.WriteLine("Main menu:");
                    Console.WriteLine("1. Login");
                    Console.WriteLine("2. Register new user");
                    string type = Console.ReadLine();
                    switch (type)
                    {
                        case "1":
                            Login();
                            break;
                        case "2":
                            Register();
                            break;
                        default:
                            PrintIncorrectMenuError();
                            break;
                    }
                }
                else
                {
                    int messagesCount = mailManager.GetNewMessagesCount(username, token);
                    if (messagesCount > 0)
                    {
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.WriteLine("You have {0} new messages.", messagesCount);
                        Console.ForegroundColor = ConsoleColor.Gray;
                    }

                    Console.WriteLine("Main menu:");
                    Console.WriteLine("1. Mail");
                    Console.WriteLine("2. Change password");
                    Console.WriteLine("3. Delete me");
                    Console.WriteLine("4. Logout");
                    string type = Console.ReadLine();
                    switch (type)
                    {
                        case "1":
                            Mail();
                            break;
                        case "4":
                            Logout();
                            break;
                        case "2":
                            ChangePassword();
                            break;
                        case "3":
                            DeleteUser();
                            break;
                        default:
                            PrintIncorrectMenuError();
                            break;
                    }
                }
            }
        }

        private static void Mail()
        {
            while (true)
            {
                Console.WriteLine("Mail:");
                Console.WriteLine("1. Check for new messages");
                Console.WriteLine("2. Read new messages");
                Console.WriteLine("3. Read all messages");
                Console.WriteLine("4. Send message");
                Console.WriteLine("5. Back");
                string type = Console.ReadLine();
                switch (type)
                {
                    case "1":
                        CheckForNewMail();
                        break;
                    case "2":
                        ReadNewMail();
                        break;
                    case "3":
                        ReadOldMail();
                        break;
                    case "4":
                        SendMessage();
                        break;
                    case "5":
                        return;
                    default:
                        PrintIncorrectMenuError();
                        break;
                }
            }
        }

        private static void CheckForNewMail()
        {
            int messagesCount = mailManager.GetNewMessagesCount(username, token);
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("You have {0} new messages.", messagesCount);
            Console.ForegroundColor = ConsoleColor.Gray;
        }


        private static void SendMessage()
        {
            Console.Write("To user name:");
            string toUserName = Console.ReadLine();
            Console.Write("Message:");
            string message = Console.ReadLine();
            mailManager.SendNewMessage(username, toUserName, message, token);
        }

        private static void ReadNewMail()
        {
            List<MessageSimple> messages = mailManager.GetNewMessages(username, token);
            if (messages.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("You have no new messages.");
                Console.ForegroundColor = ConsoleColor.Gray;
                return;
            }

            for (int i = 0; i < messages.Count; i++)
            {

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(new string('-', 70));
                Console.WriteLine("Message {0} from {1}:", i+1, messages.Count);
                Console.WriteLine(new string('-', 70));
                Console.WriteLine("From: {0}", messages[i].fromUser);
                Console.WriteLine("To: {0}", messages[i].toUser);
                Console.WriteLine("Date: {0}", messages[i].Date);
                Console.WriteLine(new string('-', 70));
                Console.WriteLine(messages[i].Text);
                Console.WriteLine(new string('-', 70));
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Delete message [Y/N]?");
                char key = Console.ReadKey().KeyChar;
                Console.WriteLine();
                if (key == 'Y' || key == 'y') mailManager.DeleteMessage(username, messages[i].Id, token);
                else mailManager.SetMessageReaded(username, messages[i].Id, token);
            }
        }

        private static void ReadOldMail()
        {
            List<MessageSimple> messages = mailManager.GetAllMessages(username, token);
            if (messages.Count == 0)
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine("You have no messages.");
                Console.ForegroundColor = ConsoleColor.Gray;
                return;
            }

            for (int i = 0; i < messages.Count; i++)
            {

                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(new string('-', 70));
                Console.WriteLine("Message {0} from {1}:", i+1, messages.Count);
                if (!messages[i].Received)
                {
                    Console.ForegroundColor = ConsoleColor.Cyan;
                    Console.WriteLine("This is new message.");
                    Console.ForegroundColor = ConsoleColor.White;
                }
                Console.WriteLine(new string('-', 70));
                Console.WriteLine("From: {0}", messages[i].fromUser);
                Console.WriteLine("To: {0}", messages[i].toUser);
                Console.WriteLine("Date: {0}", messages[i].Date);
                Console.WriteLine(new string('-', 70));
                Console.WriteLine(messages[i].Text);
                Console.WriteLine(new string('-', 70));
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine("Delete message [Y/N]?");
                char key = Console.ReadKey().KeyChar;
                Console.WriteLine();
                if (key == 'Y' || key == 'y') mailManager.DeleteMessage(username, messages[i].Id, token);
                else
                {
                    if (!messages[i].Received) mailManager.SetMessageReaded(username, messages[i].Id, token);
                }
            }
        }

        private static void ChangePassword()
        {
            Console.Write("Old password:");
            string oldPassword = Console.ReadLine();
            Console.Write("New Password:");
            string newPassword = Console.ReadLine();
            usersManager.ChangePassword(username, oldPassword, newPassword);
        }

        private static void DeleteUser()
        {
            Console.Write("Password:");
            string password = Console.ReadLine();
            logined = !usersManager.DeleteUser(username, password);
        }

        private static void Logout()
        {
            logined = false;
            usersManager.Exit(username, token);
        }

        private static void Register()
        {
            Console.Write("Username:");
            string username = Console.ReadLine();
            Console.Write("Password:");
            string password = Console.ReadLine();
            usersManager.CreateUser(username, password);
        }

        private static void Login()
        {
            Console.Write("Username:");
            username = Console.ReadLine();
            Console.Write("Password:");
            string password = Console.ReadLine();

            token = usersManager.Login(username, password);
            if (token != null) logined = true;
            else logined = false;
        }

        static void PrintError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        static void PrintIncorrectMenuError()
        {
            PrintError("Incorrect! Please, type menu option number and press Enter.");
        }
    }
}
