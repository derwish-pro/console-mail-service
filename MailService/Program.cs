﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Mail;
using Users;
using SqlDB;

namespace MailService
{
    class Program
    {
        static void Main(string[] args)
        {
            IRegisteredUsersDB registeredUsersDb = new SqlRegisteredUsersDb();
            IConnectedUsersDb connectedUsersDb = new SqlConnectedUsersDb();
            IMailDb mailDb = new SqlMailDb();

            //connectedUsersDb.DropDatabase();

            var usersManager = new UsersManager(registeredUsersDb, connectedUsersDb);
            var mailManager = new MailManager(usersManager,mailDb);


            ConsoleUI.Start(usersManager, mailManager);

        }
    }
}
