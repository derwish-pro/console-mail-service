﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Users;
using SqlDB;

namespace MailServiceMonitoring
{
    class Program
    {
        static void Main(string[] args)
        {
            var registeredUsersDb = new SqlRegisteredUsersDb();
            var connectedUsersDb = new SqlConnectedUsersDb();
            var usersManager = new UsersManager(registeredUsersDb, connectedUsersDb);
            while (true)
            {
                Console.Clear();
                usersManager.PrintRegisteredUsers();
                Console.WriteLine();
                usersManager.PrintLogginedUsers();
                Thread.Sleep(1000);
            }
        }
    }
}
