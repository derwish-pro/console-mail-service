﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users
{
    public class MessageSimple
    {
        /*
         * Этот класс я создал потому что 
         * сообщение передается пользователю в виде экземпляра класса
         * и если ему передавать Message в том виде, в котором он есть
         * то пользователь получит доступ к полю fromUser через которое
         * можно получить профайл пользователя со всеми его данными
         * 
         * */

        public int Id { get; set; }
        public bool Received { get; set; }
        public DateTime? Date { get; set; }
        public string fromUser { get; set; }
        public string toUser { get; set; }
        public string Text { get; set; }
    }
}
