﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Users
{
    public partial class ConnectedUser
    {

        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Token { get; set; }
        public DateTime? LoginTime { get; set; }
        [Required]
        public virtual RegisteredUser RegisteredUser { get; set; }
    }
}
