﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Users;

namespace Users
{
    public interface IMailDb
    {
        bool CreateNewMessage(string fromUserName, string toUserName, string text);
        List<Message> GetNewMessages(string userName);
        List<Message> GetAllMessages(string userName);
        bool DeleteMessage(int messageId);
        bool SetMessageReaded(int messageId);
    }
}
