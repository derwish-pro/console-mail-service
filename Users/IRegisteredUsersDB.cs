﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;


namespace Users
{
    public interface IRegisteredUsersDB
    {
        bool CreateNewUser(string userName, string password);
        bool DeleteUser(string userName);
        RegisteredUser GetUser(string userName);
        List<RegisteredUser> GetAllUsers();
        bool ChangeLoginDate(string userName, DateTime datetime);
        bool ChangePassword(string userName, string password);
        bool DropDatabase();
    }
}
