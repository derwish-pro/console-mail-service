﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Users
{
    public interface IConnectedUsersDb
    {
        bool AddUser(string userName, string token );
        bool RemoveUser(string userName);
        ConnectedUser GetUser(string userName);
        List<ConnectedUser> GetAllUsers();
        bool DropDatabase();
    }
}
