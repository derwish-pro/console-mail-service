﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Users
{
    public partial class RegisteredUser
    {
        public RegisteredUser()
        {
            this.sendedMails = new HashSet<Message>();
            this.receivedMails = new HashSet<Message>();
        }


        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public virtual ConnectedUser ConnectedUser { get; set; }
        public virtual ICollection<Message> sendedMails { get; set; }
        public virtual ICollection<Message> receivedMails { get; set; }
    }
}
