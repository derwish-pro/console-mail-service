﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Users
{
    public class UsersManager
    {

        private IRegisteredUsersDB registeredUsers;
        private IConnectedUsersDb connectedUsers;

        public UsersManager(IRegisteredUsersDB registeredUsers, IConnectedUsersDb connectedUsers)
        {
            this.registeredUsers = registeredUsers;
            this.connectedUsers = connectedUsers;
            SetPrintLogMethodsToConsole();
        }

        #region Logs

        public delegate void PrintGoodLogDelegate(string message);
        public delegate void PrintErrorLogDelegate(string message);

        private PrintGoodLogDelegate printGoodLogMethod;
        private PrintErrorLogDelegate printErrorLogMethod;

        public void SetPrintGoodLogMethod(PrintGoodLogDelegate deletate)
        {
            printGoodLogMethod = deletate;
        }

        public void SetPrintLogMethodsToConsole()
        {
            SetPrintErrorLogMethod(message =>
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
            });

            SetPrintGoodLogMethod(message =>
            {
                Console.ForegroundColor = ConsoleColor.Cyan;
                Console.WriteLine(message);
                Console.ForegroundColor = ConsoleColor.Gray;
            });
        }



        public void SetPrintErrorLogMethod(PrintErrorLogDelegate deletate)
        {
            printErrorLogMethod = deletate;
        }

        private void PrintGoodLog(string messgae)
        {
            if (printGoodLogMethod != null)
                printGoodLogMethod(messgae);
        }

        private void PrintErrorLog(string messgae)
        {
            if (printErrorLogMethod != null)
                printErrorLogMethod(messgae);
        }

        #endregion

        #region Manage RegisteredUser

        public bool CheckUserNameIsValid(string userName)
        {
            Regex objAlphaPattern = new Regex(@"^[a-zA-Z0-9_@.-]*$");
            return (objAlphaPattern.IsMatch(userName) && userName.Length > 0);
        }

        public bool CreateUser(string userName, string password)
        {
            if (!CheckUserNameIsValid(userName))
            {
                PrintErrorLog(String.Format("Can`t create user. Bad username: \"{0}\".", userName));
                return false;
            }

            RegisteredUser registeredUser = GetRegisteredUser(userName);

            if (registeredUser != null)
            {
                PrintErrorLog(String.Format("Can`t create user. User \"{0}\" already exist.", userName));
                return false;
            }

            bool result = registeredUsers.CreateNewUser(userName, password);
            if (result) PrintGoodLog(String.Format("User \"{0}\" created.", userName));
            else PrintErrorLog(String.Format("Can`t create user \"{0}\".", userName));

            return result;
        }

        public bool DeleteUser(string userName, string password)
        {
            RegisteredUser registeredUser = GetRegisteredUser(userName);
            if (registeredUser == null)
            {
                PrintErrorLog(String.Format("Can`t delete user. User \"{0}\" not found.", userName));
                return false;
            }

            if (CheckPassword(registeredUser, password))
            {
                ConnectedUser connectedUser = GetConnectedUser(userName);
                if (connectedUser != null) Exit(userName);

                bool result = registeredUsers.DeleteUser(userName);
                if (result) PrintGoodLog(String.Format("User \"{0}\" deleted.", userName));
                else PrintErrorLog(String.Format("Can`t delete \"{0}\" user.", userName));
                return result;
            }
            else
            {
                PrintErrorLog(String.Format("Can`t delete user \"{0}\". Password is wrong.", userName));
                return false;
            }

        }


        /// <summary>
        /// Try to Login and return token
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public string Login(string userName, string password)
        {
            RegisteredUser registeredUser = GetRegisteredUser(userName);

            if (registeredUser == null)
            {
                PrintErrorLog(String.Format("Can`t login. User \"{0}\" not found", userName));
                return null;
            }

            if (CheckPassword(registeredUser, password))
            {
                ConnectedUser connectedUser = GetConnectedUser(userName);

                if (connectedUser != null)
                {
                    PrintErrorLog(String.Format("User \"{0}\" is already logined. Initiating exit...", userName));
                    Exit(userName);
                }

                string token = CreateNewToken();

                if (connectedUsers.AddUser(userName, token))
                {
                    PrintGoodLog(String.Format("User \"{0}\" logined.", userName));
                    return token;
                }
                else
                {
                    PrintErrorLog(String.Format("Can`t login user \"{0}\".", userName));
                    return null;
                }
            }
            else
            {
                PrintErrorLog(String.Format("Can`t login user \"{0}\". Password is wrong.", userName));
                return null;
            }
        }

        private string CreateNewToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            string token = Convert.ToBase64String(time.Concat(key).ToArray());
            return token;
        }



        //public bool Exit(string userName, string password)
        //{
        //    ConnectedUser connectedUser = GetConnectedUser(userName);
        //    if (connectedUser == null)
        //    {
        //        PrintErrorLog(String.Format("Can`t exit. User \"{0}\" is not logined.", userName));
        //        return false;
        //    }

        //    RegisteredUser registeredUser = GetRegisteredUser(userName);
        //    if (CheckPassword(registeredUser, password))
        //    {
        //        return Exit(userName);
        //    }
        //    else
        //    {
        //        PrintErrorLog(String.Format("Can`t login user \"{0}\". Password is wrong.", userName));
        //        return false;
        //    }
        //}

        public bool Exit(string userName, string token)
        {
            ConnectedUser connectedUser = GetConnectedUser(userName);
            if (connectedUser == null)
            {
                PrintErrorLog(String.Format("Can`t exit. User \"{0}\" is not logined.", userName));
                return false;
            }

            if (connectedUser.Token == token)
            {
                return Exit(userName);
            }
            else
            {
                PrintErrorLog(String.Format("Can`t exit user \"{0}\". Token is wrong.", userName));
                return false;
            }
        }

        private bool Exit(string userName)
        {
            bool result = (connectedUsers.RemoveUser(userName));
            if (result) PrintGoodLog(String.Format("User \"{0}\" exit.", userName));
            else PrintErrorLog(String.Format("Can`t exit user \"{0}\".", userName));

            return result;
        }


        private bool ChangePassword(string userName, string password)
        {

            bool result = registeredUsers.ChangePassword(userName, password);
            if (result) PrintGoodLog(String.Format("User \"{0}\" password changed.", userName));
            else PrintErrorLog(String.Format("Can`t change password for \"{0}\" user.", userName));

            return result;
        }

        public bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            RegisteredUser registeredUser = GetRegisteredUser(userName);
            if (registeredUser == null)
            {
                PrintErrorLog(String.Format("Can`t change password. User \"{0}\" not found.", userName));
                return false;
            }

            if (CheckPassword(registeredUser, oldPassword))
            {
                return ChangePassword(userName, newPassword);
            }
            else
            {
                PrintErrorLog(String.Format("Can`t change password. User \"{0}\" old password is wrong.", userName));
                return false;
            }
        }

        #endregion

        #region Get user info


        private bool CheckPassword(RegisteredUser registeredUser, string password)
        {
            return registeredUser.Password == password;
        }


        private RegisteredUser GetRegisteredUser(string userName)
        {
            return registeredUsers.GetUser(userName);
        }

        private ConnectedUser GetConnectedUser(string userName)
        {
            return connectedUsers.GetUser(userName);
        }

        public bool CheckToken(string userName, string token)
        {
            ConnectedUser user = connectedUsers.GetUser(userName);
            if (user==null) return false;
            return user.Token == token;

        }

        #endregion

        #region Print users info
        public void PrintLogginedUsers()
        {
            List<ConnectedUser> users = connectedUsers.GetAllUsers();

            if (users.Count == 0)
            {
                Console.WriteLine("No logined users.");
                return;
            }

            var sortedUsers = from user in users
                              orderby user.LoginTime ascending
                              select user;
            users = sortedUsers.ToList();

            Console.WriteLine("Logined {0} users:", users.Count);
            Console.WriteLine(new string('-', 79));
            for (int i = 0; i < users.Count; i++)
            {
                Console.WriteLine("{0}: {1} : {2}",
                    i,
                    users[i].LoginTime,
                    users[i].Name);

            }
            Console.WriteLine(new string('-', 79));
        }

        public void PrintRegisteredUsers()
        {
            List<RegisteredUser> users = registeredUsers.GetAllUsers();

            if (users.Count == 0)
            {
                Console.WriteLine("No registered users");
                return;
            }

            var sortedUsers = from user in users
                              orderby user.RegistrationDate ascending
                              select user;
            users = sortedUsers.ToList();

            Console.WriteLine("Registered {0} users:", users.Count);
            Console.WriteLine(new string('-', 79));
            for (int i = 0; i < users.Count; i++)
            {
                string logindate = users[i].LastLoginDate.ToString();
                if (logindate == "") logindate = "-------------------";

                Console.WriteLine("{0}: reg {1} : log {2} : {3} ",
                      i,
                      users[i].RegistrationDate,
                      logindate,
                      users[i].Name
                      );

            }
            Console.WriteLine(new string('-', 79));
        }

        #endregion


    }
}
