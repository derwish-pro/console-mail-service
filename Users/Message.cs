﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using Users;

namespace Users
{
    public class Message
    {
        public int Id { get; set; }
        public bool Received { get; set; }
        public DateTime? Date { get; set; }
        public virtual RegisteredUser fromUser { get; set; }
        public virtual RegisteredUser toUser { get; set; }
        public string Text{ get; set; }
    }
}
