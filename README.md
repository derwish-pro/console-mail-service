# Console Mail Service #

This is an example of work with Entity Framework in a console application. The program allows you to create users, authenticate them and send messages between users. In a separate window, the system administrator can monitor user activity.